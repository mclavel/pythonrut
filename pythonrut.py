import random

def verificar_rut(rut_full):
	try:
		rut_cort = rut_full.split("-")
		dig_ver = rut_cort[1]
		resto = rut_cort[0].replace(".","").replace("-","").replace("-","")
	except:
		print "Error, formato rut incorrecto. Ej: 123456789-0"
		return False

	if dig_ver.lower() == digito_verificador(resto):
		return True
	return False

def digito_verificador(rut_split):
	if not rut_split.isdigit():
		print "Error, rut con letras."
		return False
	else:
		aux = 2
	        num_acum = 0
        	for i in reversed(rut_split):
	                num_acum = num_acum + (aux * int(i))
	                aux = aux + 1
        	        if aux==8:
                	        aux=2
	        res = num_acum%11
		num_final = 11 - res
	        if num_final == 11:
        	        dig_final = "0"
	        elif num_final == 10:
        	        dig_final = "k"
	        else:
        	        dig_final = str(num_final)
		return dig_final


def rut_aleatorio():
	cuerpo = random.randint(999999,99999999)
	dig = digito_verificador(str(cuerpo))
	return str(cuerpo)+"-"+dig
